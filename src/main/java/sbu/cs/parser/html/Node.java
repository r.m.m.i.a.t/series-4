package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {

    private List<Node> Tags = new ArrayList<>();
    private String Stag;
    private Map<String , String> keyValue = new HashMap<>();


    public Node(String Stag)
    {
             this.Stag = Stag;
    }

    public void setTags(Node tag)
    {
        Tags.add(tag);
    }

    public String getTags()
    {
        return Stag;
    }

    public String getTagName (String Stag)
    {
        int openingIndex = Stag.indexOf("<") ;
        int closingIndex = Stag.indexOf(">");
        return Stag.substring(openingIndex + 1 , closingIndex );
    }

    public boolean isEnd(String Stag )
    {
        if((Stag.indexOf("/") - Stag.indexOf("<"))==1)
        {
            return false ;
        }
        return true ;
    }


    @Override
    public String getStringInside()
    {
        if(!isEnd(Stag))
        {
            return null;
        }


        String openingTag = "<" + getTagName(Stag) + ">";
        String closingTag = "</" + getTagName(Stag) + ">";

        int openingIndex = Stag.indexOf(openingTag);
        int closingIndex = Stag.indexOf(closingTag);

        String ans = Stag.substring(openingIndex + openingTag.length() , closingIndex  );

        return ans ;
    }


    @Override
    public List<Node> getChildren()
    {
        for(int i = 0 ; i < Tags.size() ; i++)
        {
            Node tag= HTMLParser.parse(Tags.get(i).Stag);
            Tags.set(i , tag);
        }
        return Tags;
    }

    @Override
    public String getAttributeValue(String key)
    {
        int openingIndex = Stag.indexOf(" ");
        int closingIndex = Stag.indexOf(">");

        String tmp = Stag.substring(openingIndex + 1 , closingIndex);
        tmp = tmp.replaceAll("\" " , "!");



        for ( String value : tmp.split("!"))
        {
            String[] splits = value.split("=") ;
            keyValue.put(splits[0].replaceAll("\"" , "") , splits[1].replaceAll("\"" , ""));
        }

        return keyValue.get(key);
 }


//    /*
//    * this function will return all that exists inside a tag
//    * for example for <html><body><p>hi</p></body></html>, if we are on
//    * html tag this function will return <body><p1>hi</p1></body> and if we are on
//    * body tag this function will return <p1>hi</p1> and if we are on
//    * p tag this function will return hi
//    * if there is nothing inside tag then null will be returned
//     */
//    @Override
//    public String getStringInside() {
//        // TODO implement this
//        return null;
//    }
//
//    /*
//    *
//     */
//    @Override
//    public List<Node> getChildren() {
//        return null;
//    }
//
//    /*
//    * in html tags all attributes are in key value shape. this function will get a attribute key
//    * and return it's value as String.
//    * for example <img src="img.png" width="400" height="500">
//     */
//    @Override
//    public String getAttributeValue(String key) {
//        // TODO implement this
//        return null;
//    }
}
