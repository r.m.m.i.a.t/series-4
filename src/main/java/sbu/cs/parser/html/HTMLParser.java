package sbu.cs.parser.html;

public class HTMLParser {


    public static Node parse(String document)
    {
        Node node = new Node(document);

        document = document.replaceAll("\n  " , "");
        document = deleteThisTag ( document );

        while( document.contains("<") )
        {
            node.setTags( returnFullTag (document) );
            document = deleteTag ( document );
        }

        return node;
    }


    public static String returnTag (String document)
    {
        int openingIndex = document.indexOf("<")+1;
        int closingIndex = document.indexOf(">");

        String ans = document.substring(openingIndex , closingIndex);

        return ans ;
    }
    public static Node returnFullTag(String document)
    {
        if(!isEnd(document))
        {
            int openingIndex = document.indexOf("<");
            int closingIndex = document.indexOf(">");

            String ans = document.substring(openingIndex , closingIndex + 1 );
            Node node = new Node(ans);

            return node;
        }
        else
        {
            if(haveAtribute (document))
            {
                int atributeOpeningIndex = document.indexOf("<")+1;
                int atributeClosingIndex = document.indexOf(" ");

                String openingTag = "<" + returnTag (document) + ">";
                String closingTag = document.substring(atributeOpeningIndex , atributeClosingIndex);
                closingTag = "</" + closingTag + ">";

                int openingIndex = document.indexOf(openingTag);
                int closingIndex = document.indexOf(closingTag);

                String ans = document.substring( openingIndex , closingIndex + closingTag.length());
                Node node = new Node(ans);

                return node;
            }
            else
            {
                String openingTag = "<" + returnTag (document) + ">";
                String closingTag = "</" + returnTag (document) + ">";

                int openingIndex = document.indexOf(openingTag);
                int closingIndex = document.indexOf(closingTag);

                String ans = document.substring(openingIndex , closingIndex+closingTag.length());
                Node node = new Node(ans);

                return node;
            }
        }
    }
    public static String deleteThisTag (String document)
    {
        if( haveAtribute (document) )
        {
            int openingIndex = document.indexOf("<")+1;
            int closingIndex = document.indexOf(" ");

            String openingTag = "<" + returnTag (document) + ">";
            String closingTag = "</" + document.substring(openingIndex , closingIndex) + ">";

            return document.replaceAll(openingTag , "").replaceAll(closingTag , "");
        }
        else
        {
            String openingTag = "<" + returnTag (document) + ">";
            String closingTag = "</" + returnTag (document) + ">";

            return  document.replaceAll(openingTag , "").replaceAll(closingTag , "");
        }
    }


    public static String deleteTag (String document)
    {
        String Tag = returnFullTag (document).getTags();
        String regex = "</[a-z]+>" ;
        if(Tag.matches(regex))
        {
            return document.substring(Tag.length());
        }

        return document.replaceAll(Tag , "");
    }



    public static boolean haveAtribute (String document)
    {
        int openingIndex = document.indexOf("<")+1;
        int closingIndex = document.indexOf(">");
        if(document.substring( openingIndex , closingIndex ).contains( "=" ))
        {
            return true;
        }
        return false;
    }

    public static boolean isEnd(String document)
    {

        if((document.indexOf("/")-document.indexOf("<")) == 1)
        {
            return false ;
        }
        return true ;
    }



//
//    /*
//    * this function will get a String and returns a Dom object
//     */
//    public static Node parse(String document) {
//        // TODO implement this
//        return null;
//    }
//
//    /*
//    * a function that will return string representation of dom object.
//    * only implement this after all other functions been implemented because this
//    * impl is not required for this series of exercises. this is for more score
//     */
//    public static String toHTMLString(Node root) {
//        // TODO implement this for more score
//        return null;
//    }
}
