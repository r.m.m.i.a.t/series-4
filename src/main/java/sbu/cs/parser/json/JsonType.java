package sbu.cs.parser.json;

public class JsonType {

    private String value;
    private String key;

    public void setKey(String key)
    {
        this.key = key;
    }
    public String getKey()
    {
        return key;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    public String getValue()
    {
        if(value.indexOf("[") >=0)
        {
            value = value.replaceAll("-" , ", ");
        }
        return value;
    }
}
