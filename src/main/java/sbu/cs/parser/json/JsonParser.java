package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    static List<JsonType> Elements = new ArrayList<>();

    public static Json parse(String data) {
        String key , value;
        String regex = "(\\{|\"|\\s|})" ;

        data = data.replaceAll( regex , "");

        String tmp = data;
        while (tmp.indexOf("[") >= 0) {
            int beginingIndex = tmp.indexOf("[");
            int endingIndex = tmp.indexOf("]") + 1;

            String array = tmp.substring(beginingIndex, endingIndex);
            String newArray = array.replaceAll(",", "-");

            data = data.replace(array, newArray);
            tmp = tmp.replace(array, "");
        }


        String[] keyValue = data.split(",");
        seperate(keyValue);

        Json json = new Json(Elements);
        return json;
    }

    public static void seperate(String[] keyValue) {
        for (String KV : keyValue) {

            String[] str = KV.split(":");

            JsonType json = new JsonType();

            json.setValue((str[1]));
            json.setKey(str[0]);

            Elements.add(json);
        }
    }
}
//    /*
//    * this function will get a String and returns a Json object
//     */
//    public static Json parse(String data) {
//        // TODO implement this
//        return null;
//    }
//
//    /*
//    * this function is the opposite of above function. implementing this has no score and
//    * is only for those who want to practice more.
//     */
//    public static String toJsonString(Json data) {
//        return null;
//    }

