package sbu.cs.parser.json;

import java.util.List;

public class Json implements JsonInterface {

    List<JsonType> Elements ;

    public Json(List<JsonType> Elements)
    {
        this.Elements = Elements;
    }

    @Override
    public String getStringValue(String key)
    {
        for(int i = 0 ; i < (Elements.size()) ; i++)
        {
            if(Elements.get(i).getKey().equals(key))
            {
                return Elements.get(i).getValue();
            }
        }
        return null;
    }
//    @Override
//    public String getStringValue(String key) {
//        // TODO implement this
//        return null;
//    }
}
